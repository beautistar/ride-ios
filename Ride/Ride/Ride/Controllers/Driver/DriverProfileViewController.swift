//
//  DriverProfileViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/7.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class DriverProfileViewController: BaseViewController {
    
    weak var delegate: LeftMenuProtocol?

    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var tfCounty: UITextField!
    @IBOutlet weak var tfTownship: UITextField!
    @IBOutlet weak var tfSchool: UITextField!
    @IBOutlet weak var tfBus: UITextField!
    @IBOutlet weak var switchStart: UISwitch!
    
    var schoolList = [MarkerAnnotation]()
    var busList = [BusModel]()
    
    var user = UserModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor(hex: Const.COLOR_WHITE)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        initUserData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUserData() {
        
        
        user = currentUser!
        
        lblWelcome.text = "Welcome, " + user.first_name + " " + user.last_name
        tfSchool.text = user.school_name
        tfBus.text = user.bus_no
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getSchoolDistrict(school_id: currentUser!.school_id)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    //MARK: - Button Action

    
    @IBAction func schoolAction(_ sender: Any) {
        
        var rows = [String]()
        for area in schoolList {
            rows.append(area.name)
        }
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_SCHOOL, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfSchool.text = index as? String
            self.tfBus.text = ""
            self.user.school_id = self.schoolList[value].id
            self.user.school_name = index as! String
            
            print("--selected school id : \(self.user.county_id) ----")
            print("--selected school name : \(self.user.county_name) ----")
            
            self.getBus(school_id: self.user.school_id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    
    }
    
    @IBAction func busAction(_ sender: Any) {
        
        var rows = [String]()
        for area in busList {
            rows.append(area.number!)
        }
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECTBUS, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfBus.text = index as? String
            self.user.bus_id = self.busList[value].id!
            self.user.bus_no = index as! String
            
            print("--selected bus id : \(self.user.bus_id) ----")
            print("--selected bus no : \(self.user.bus_no) ----")
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    

    @IBAction func switchAction(_ sender: Any) {
        
        user.is_started = switchStart.isOn ? Const.IS_STARTED : Const.IS_NOT_STARTED
        
        if isValid() {
            doUpdate()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.changeViewController(LeftMenu.main)
    }
    
    //MARK: - Private method
    
    func isValid() -> Bool {
        
        if tfState.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_STATE, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfCounty.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_COUNTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfTownship.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_TOWNSHIP, positive: Const.OK, negative: nil)
            return false
        }
        
        
        if tfSchool.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_SCHOOL, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfBus.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_BUS, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    func getSchoolDistrict(school_id: Int) {
        
        self.schoolList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getSchoolDistrict(school_id) { (res_code, schools, state_name, county_name, township_name) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.schoolList = schools
                self.tfState.text = state_name
                self.tfCounty.text = county_name
                self.tfTownship.text = township_name
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getBus(school_id: Int) {
        
        self.busList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getBus(school_id) { (res_code, buses) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.busList = buses
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func doUpdate() {
        
        self.showLoadingView()
        
        ApiRequest.updateDriver(user) { (message) in
            
            self.hideLoadingView()
            
            if message == Const.RESULT_SUCCESS {
                
                //TODO: - Ride start
                currentUser?.school_id = self.user.school_id
                currentUser?.school_name = self.user.school_name
                currentUser?.bus_id = self.user.bus_id
                currentUser?.bus_no = self.user.bus_no
                currentUser?.is_started = self.user.is_started
                ride_started = currentUser?.is_started == Const.IS_STARTED ? true : false
                if ride_started {
                    self.showToast(Const.MSG_START_RIDE)
                } else {
                    self.showToast(Const.MSG_STOP_RIDE)
                }
                
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
            }
        }        
    }
}
