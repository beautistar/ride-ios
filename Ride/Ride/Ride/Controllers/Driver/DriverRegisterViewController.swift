//
//  DriverRegisterViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/7.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class DriverRegisterViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var photoBgView: UIView!
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var tfCounty: UITextField!
    @IBOutlet weak var tfTownship: UITextField!
    @IBOutlet weak var tfSchool: UITextField!
    @IBOutlet weak var tfBus: UITextField!
    @IBOutlet weak var tfCode: UITextField!
    
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgUrl: String = ""
    
    var stateList = [AreaModel]()
    var countyList = [AreaModel]()
    var townshipList = [AreaModel]()
    var schoolList = [MarkerAnnotation]()
    var busList = [BusModel]()
    
    var user = UserModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        photoBgView.layer.borderColor = UIColor.white.cgColor
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        self.navigationItem.title = Const.TITLE_DRV_REG
        
        self._picker.delegate = self
        _picker.allowsEditing = true
        
        getState()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Button Action
    @IBAction func attacAction(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            || UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let galleryAction: UIAlertAction = UIAlertAction(title: Const.FROM_GALLERY, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let cameraAction: UIAlertAction = UIAlertAction(title: Const.FROM_CAMERA, style: UIAlertActionStyle.default, handler: { (cameraSourceAlert) in
                self._picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self._picker, animated: true, completion: nil)
            })
            
            photoSourceAlert.addAction(galleryAction)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                photoSourceAlert.addAction(cameraAction)
            }
            photoSourceAlert.addAction(UIAlertAction(title: Const.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            photoSourceAlert.popoverPresentationController?.sourceView = photoBgView
            photoSourceAlert.popoverPresentationController?.sourceRect = photoBgView.bounds

            
            self.present(photoSourceAlert, animated: true, completion: nil);
        }
    }
    
    @IBAction func stateAction(_ sender: Any) {
        
        var rows = [String]()
        for area in stateList {
            rows.append(area.name)
        }
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECTSTATE, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfState.text = index as? String
            self.tfCounty.text = ""
            self.tfTownship.text = ""
            self.tfSchool.text = ""
            self.tfBus.text = ""
            self.user.state_id = self.stateList[value].id
            self.user.state_name = index as! String
            
            print("--selected state id : \(self.user.state_id) ----")
            print("--selected state name : \(self.user.state_name) ----")
            
            self.getCounty(state_id: self.stateList[value].id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func countyAction(_ sender: Any) {
        
        var rows = [String]()
        for area in countyList {
            rows.append(area.name)
        }
        
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_COUNTY, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfCounty.text = index as? String
            self.tfTownship.text = ""
            self.tfSchool.text = ""
            self.tfBus.text = ""
            self.user.county_id = self.countyList[value].id
            self.user.county_name = index as! String
            
            print("--selected county id : \(self.user.county_id) ----")
            print("--selected county name : \(self.user.county_name) ----")
            
            self.getTownship(county_id: self.user.county_id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func townshipAction(_ sender: Any) {
        
        var rows = [String]()
        for area in townshipList {
            rows.append(area.name)
        }
        
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_TOWNSHIP, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfTownship.text = index as? String
            self.tfSchool.text = ""
            self.tfBus.text = ""
            self.user.township_id = self.townshipList[value].id
            self.user.township_name = index as! String
            
            print("--selected township id : \(self.user.county_id) ----")
            print("--selected township name : \(self.user.county_name) ----")
            
            self.getSchool(township_id: self.user.township_id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func schoolAction(_ sender: Any) {
        
        var rows = [String]()
        for area in schoolList {
            rows.append(area.name)
        }
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_SCHOOL, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfSchool.text = index as? String
            self.tfBus.text = ""
            self.user.school_id = self.schoolList[value].id
            self.user.school_name = index as! String
            
            print("--selected school id : \(self.user.county_id) ----")
            print("--selected school name : \(self.user.county_name) ----")
            
            self.getBus(school_id: self.user.school_id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func busAction(_ sender: Any) {
        
        var rows = [String]()
        for area in busList {
            rows.append(area.number!)
        }
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECTBUS, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfBus.text = index as? String
            self.user.bus_id = self.busList[value].id!
            self.user.bus_no = index as! String
            
            print("--selected bus id : \(self.user.bus_id) ----")
            print("--selected bus no : \(self.user.bus_no) ----")
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        if isValid() {
            self.doRegister()
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVC.user_type = Const.DRIVER
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - ImagePicker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imvProfile.image = pickedImage
            _imgUrl = saveToFile(image: pickedImage, filePath: Const.SAVE_ROOT_PATH, fileName: "profile.png")
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Private method
    
    func isValid() -> Bool {
        
        if _imgUrl.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PHOTO, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfFirstName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_FIRSTNAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfLastName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_LASTNAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_INVALID, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPhone.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PHONE_NO_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfState.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_STATE, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfCounty.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_COUNTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfTownship.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_TOWNSHIP, positive: Const.OK, negative: nil)
            return false
        }
        
        
        if tfSchool.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_SCHOOL, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfBus.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_BUS, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfCode.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_CODE_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    func getState() {
        
        self.stateList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getState { (res_code, area) in
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.stateList = area
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getCounty(state_id: Int) {
        
        self.countyList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getCounty(state_id) { (res_code, counties) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.countyList = counties
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }       
    }
    
    func getTownship(county_id: Int) {
        
        self.townshipList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getTownship(county_id) { (res_code, townships) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.townshipList = townships
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getSchool(township_id: Int) {
        
        self.schoolList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getSchool(township_id) { (res_code, schools) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.schoolList = schools
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getBus(school_id: Int) {
        
        self.busList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getBus(school_id) { (res_code, buses) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.busList = buses
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func doRegister() {
        
        user.first_name = tfFirstName.text!
        user.last_name = tfLastName.text!
        user.email = tfEmail.text!
        user.phone_no = tfPhone.text!
        user.password = tfCode.text!
        
        self.showLoadingView()
        ApiRequest.registerDriver(user, imgURL: _imgUrl) { (res_code) in
            
            self.hideLoadingView()
            
            ApiRequest.saveToken(token: currentUser!.token, completion: { (code) in     
            })
            
            if res_code == Const.CODE_SUCCESS {
                self.gotoThanks()
            } else if res_code == Const.CODE_ALREADY_REGISTERED {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.EXIST_EMAIL, positive: Const.OK, negative: nil)
            } else if res_code == Const.CODE_UPLOADFAIL {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.UPLOAD_FAILED, positive: Const.OK, negative: nil)
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func gotoThanks() {
        
        let thanksVC = self.storyboard?.instantiateViewController(withIdentifier: "ThanksViewController") as! ThanksViewController
        self.navigationController?.pushViewController(thanksVC, animated: true)
    }    
}
