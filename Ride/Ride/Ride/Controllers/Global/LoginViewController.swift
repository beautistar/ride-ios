//
//  LoginViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/5.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var photoBgView: UIView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    var user_type: String = Const.STUDENT

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        photoBgView.layer.borderColor = UIColor.white.cgColor
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        if user_type == Const.STUDENT {
            self.navigationItem.title = Const.TITLE_STD_LOGIN
        } else {
            self.navigationItem.title = Const.TITLE_DRV_LOGIN
        }
        
        if let email = UserDefaults.standard.string(forKey: Const.KEY_EMAIL) {
            tfEmail.text = email
        }
        if let password = UserDefaults.standard.string(forKey: Const.KEY_PASSWORD) {
            tfPassword.text = password
        }        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private method
    
    func isValid() -> Bool {
        
        if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_INVALID, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        if isValid() {
            
            self.showLoadingView()
            
            ApiRequest.login(email: tfEmail.text!, password: tfPassword.text!, user_type: user_type) { (message) in
                self.hideLoadingView()
                
                if let user = currentUser {
                    ApiRequest.saveToken(token: user.token, completion: { (code) in
                    })
                }
               
                
                if message == Const.RESULT_SUCCESS {
                    
                    if self.user_type == Const.DRIVER {
                        if currentUser?.is_approved != Const.APPROVED {
                            self.gotoThanksVC()
                        } else {
                            self.createMenuView()
                        }
                    } else {
                        self.createMenuView()
                    }                    
                } else {
                    self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func gotoThanksVC() {
        let thanksVC = self.storyboard?.instantiateViewController(withIdentifier: "ThanksViewController") as! ThanksViewController
        self.navigationController?.pushViewController(thanksVC, animated: true)
    }
    
    fileprivate func createMenuView() {        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: Const.COLOR_WHITE)
        nvc.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        
        menuViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: menuViewController)
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        
    }

}
