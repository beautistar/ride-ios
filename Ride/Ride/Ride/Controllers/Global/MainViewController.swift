//
//  MainViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import MapKit


class MainViewController: BaseViewController, MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var user_type: String?
    var latitude: Float = 0.0
    var longitude: Float = 0.0
    var notiFlag = 0
    var locationTimer = Timer()

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var btnViewMode: UIButton!
    
    let locationManager = CLLocationManager()
    
    var searchSchools = [MarkerAnnotation]()
    var schools = [MarkerAnnotation]()

    var isStreetMode: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // show artwork on map
        
        let _duration = UserDefaults.standard.integer(forKey: Const.PARAM_DURATION)
        if _duration !=  0 {
            duration = _duration
        }

        mapView.register(MarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        mapView.delegate = self
        schoolFound = false
        tblSearch.isHidden = true
       
        navigationController?.navigationBar.isHidden = false
        
        if let user = currentUser {
            if user.user_id != 0 {
                self.updateTimer()
                self.setNavigationBarItem()
                if user.type == Const.STUDENT {
                    self.getMyDriver()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        self.navigationController?.navigationBar.tintColor = UIColor(hex: Const.COLOR_WHITE)
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor(hex: Const.COLOR_WHITE)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        isStreetMode = UserDefaults.standard.bool(forKey: Const.KEY_MAP_MODE)
        changeMapMode()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
        
        getNearbySchool()
        
        // set initial location
        let myLocation = CLLocation(latitude: Double(latitude), longitude: Double(longitude))
        centerMapOnLocation(location: myLocation)
    }
    
    @IBAction func viewModeChangeAction(_ sender: Any) {
        
        isStreetMode = !isStreetMode
        
        changeMapMode()
        
        UserDefaults.standard.set(isStreetMode, forKey: Const.KEY_MAP_MODE)
    }
    
    func changeMapMode() {
        
        if !isStreetMode {
            mapView.mapType = .satellite
            btnViewMode.setImage(UIImage(named: "ic_street"), for: .normal)
        } else {
            mapView.mapType = .standard
            btnViewMode.setImage(UIImage(named: "ic_satellite"), for: .normal)
        }        
    }
    
    // MARK: - Private method
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius);
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("access allowed")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
            //locationManager.startUpdatingLocation()
        }
    }
    
    func getNearbySchool() {
        
        ApiRequest.getNearbySchool(duration) { (res_code, schools) in
            
            if schools.count > 0 {
                schoolFound = true
                self.schools = schools
                self.mapView.addAnnotations(schools)
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.SCHOOL_NOT_FOUND, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func gotoSignupStudent(school: MarkerAnnotation) {
        
        let signupStudentVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupNameViewController") as! SignupNameViewController
        signupStudentVC.selectedSchool = school
        self.navigationController?.pushViewController(signupStudentVC, animated: true)        
    }

    
    // MARK: - MKMapViewDelegate method
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        if latitude == 0.0 || longitude == 0.0 {
            let visibleRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, regionRadius, regionRadius)
            self.mapView.setRegion(self.mapView.regionThatFits(visibleRegion), animated: true)
        }
        
        latitude = Float(userLocation.coordinate.latitude)
        longitude = Float(userLocation.coordinate.longitude)
        print("-----------------------------------------")
        print("latitude", latitude)
        print("latitude", longitude)
        
        UserDefaults.standard.set(latitude, forKey: Const.KEY_LAT)
        UserDefaults.standard.set(longitude, forKey: Const.KEY_LNG)
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let school = view.annotation as! MarkerAnnotation
        print("school tapped: ", school.title!)
        /*
        let location = view.annotation as! BusAnnotation
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
        */
        gotoSignupStudent(school: school)
    }
    
    func updateTimer() {
        locationTimer = Timer.scheduledTimer(timeInterval: updateTime, target: self, selector: #selector(updateInfo), userInfo: nil, repeats: true)
    }
    
    @objc func updateInfo() {
        
        if latitude != 0.0 && longitude != 0.0 {
            
            if let user = currentUser {
                if user.type == Const.DRIVER {
                    if user.is_started == Const.IS_STARTED {
                        updateDriverLocation()
                    }                    
                } else {
                    getMyDriver()
                }
                if !schoolFound {
                    getNearbySchool()
                }
            } else {
                if !schoolFound {
                    getNearbySchool()
                }
            }
        }
    }
    
    func updateDriverLocation() {
        
        print("-----------------------------------------")
        print("current user latitude", currentUser!.latitude)
        print("current user latitude", currentUser!.longitude)
        
        ApiRequest.updateDriverLocation { (result_code) in
            print(result_code)
        }
    }
    
    func getMyDriver() {
        
        for annotation in mapView.annotations {
            if let subtitle = annotation.subtitle {
                if subtitle?.range(of: "Bus number") != nil {
                    mapView.removeAnnotation(annotation)
                }
            }
        }
        
        ApiRequest.getMyDriver { (result_code, bus) in
            if result_code == Const.CODE_SUCCESS {
                if bus.is_started == Const.IS_STARTED {
                    self.mapView.addAnnotation(bus)
                    
                    // send notication
                    self.sendNotification(lat: bus.coordinate.latitude, lng: bus.coordinate.longitude)
                }                
            }
        }
    }
    
    func sendNotification(lat: Double, lng: Double) {
        
        let coordinate0 = CLLocation(latitude: CLLocationDegrees(currentUser!.latitude), longitude: CLLocationDegrees(currentUser!.longitude))
        let coordinate1 = CLLocation(latitude: lat, longitude: lng)
        
        let distanceInMeters = coordinate0.distance(from: coordinate1) // result is in meters
        
        print("distance : ", distanceInMeters)
        
        if Float(distanceInMeters) < Float(1609 * currentUser!.alert_mile) {
            
            if notiFlag < 3 {
                ApiRequest.sendNotification(userId: currentUser!.user_id, message: "Your bus is within " + "\(currentUser!.alert_mile)" + " mile (s)") { (resCode) in
                    self.notiFlag = self.notiFlag + 1
                }
            }
        }
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        searchSchools.removeAll()
        
        if tfSearch.text!.count > 2 {
            for school in schools {
                if school.name.lowercased().range(of: tfSearch.text!) != nil {
                    searchSchools.append(school)
                }
            }
            
            if searchSchools.count > 0 {
                tblSearch.isHidden = false
                tblSearch.reloadData()
            }
        }
    }
    
    //MARK: - TableView delegate & datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searchSchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell")
        if cell != nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "SchoolCell")
        }
        cell?.textLabel?.text = searchSchools[indexPath.row].name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("searched loc lat", searchSchools[indexPath.row].coordinate.latitude)
        print("searched loc lng", searchSchools[indexPath.row].coordinate.longitude)
        
        centerMapOnLocation(location: CLLocation(latitude: searchSchools[indexPath.row].coordinate.latitude, longitude: searchSchools[indexPath.row].coordinate.longitude))
        mapView.selectAnnotation(searchSchools[indexPath.row], animated: true)
        tblSearch.isHidden = true
        self.view.endEditing(true)
        
        tfSearch.text = ""
    }
    
    //MARK: - TextField delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        searchSchools.removeAll()
        
        if tfSearch.text!.count > 0 {
            for school in schools {
                if school.name.lowercased().range(of: tfSearch.text!) != nil {
                    searchSchools.append(school)
                }
            }
            
            if searchSchools.count > 0 {
                tblSearch.isHidden = false
                tblSearch.reloadData()
            }
        } else {
            tblSearch.isHidden = true
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        searchSchools.removeAll()
        tblSearch.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        tblSearch.isHidden = true
    }

}



extension MainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
