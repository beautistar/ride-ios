//
//  MenuViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

import SDWebImage

enum LeftMenu: Int {
    case main = 0
    case profile
    //case setting
    //case notification
    case logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class MenuViewController: UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tblMenu: UITableView!
    

    var menusNames = ["Profile", /*"Notifications",*/ "Sign Out"]

    var menuIcons = [#imageLiteral(resourceName: "ic_profile"),/* #imageLiteral(resourceName: "ic_notification"),*/ #imageLiteral(resourceName: "ic_logout")]
    
    var mainViewController: UIViewController!
    //var notificationViewController: UIViewController!
    var profileViewController: UIViewController!
    var driverProfileViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        tblMenu.tableFooterView = UIView()
        
        let mainTabViewController = storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.mainViewController = UINavigationController(rootViewController: mainTabViewController)
        
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        profileVC.delegate = self
        self.profileViewController = UINavigationController(rootViewController: profileVC)
        
        let driverProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverProfileViewController") as! DriverProfileViewController
        driverProfileVC.delegate = self
        self.driverProfileViewController = UINavigationController(rootViewController: driverProfileVC)
        
        
        //let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        //notificationVC.delegate = self
        //self.notificationViewController = UINavigationController(rootViewController: notificationVC)
        
        
        // set user data
        imvProfile.sd_setImage(with: URL(string: (currentUser?.photo_url)!), placeholderImage: #imageLiteral(resourceName: "img_user"))
        lblName.text = currentUser!.first_name + " " + currentUser!.last_name
    }
    
    // MARK: - left menu protocol
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .profile:
            if currentUser?.type == Const.STUDENT {
                self.slideMenuController()?.changeMainViewController(self.profileViewController, close: true)
            } else {
                self.slideMenuController()?.changeMainViewController(self.driverProfileViewController, close: true)
            }
        //case .notification:
        //    self.slideMenuController()?.changeMainViewController(self.notificationViewController, close: true)
        case .logout:
            logout()
        }
    }
    
    func logout() {
        
        //TODO: logoout action
        
        let user = UserModel()
        currentUser = user
        schoolFound = false
        UserDefaults.standard.set(user.password, forKey: Const.KEY_PASSWORD)
        UserDefaults.standard.set(user.email, forKey: Const.KEY_EMAIL)
        UserDefaults.standard.set(user.password, forKey: Const.KEY_PASSWORD)
        UserDefaults.standard.set(user.type, forKey: Const.KEY_USERTYPE)
        UserDefaults.standard.set(user.token, forKey: Const.KEY_TOKEN)
        
        ApiRequest.saveToken(token: "") { (code) in            
        }
        
        let loginNav = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = loginNav
    }
}

extension MenuViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .profile, /*.notification,*/ .logout:
                return 50
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row+1) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblMenu == scrollView {
            
        }
    }
}

extension MenuViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menusNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .profile, /*.notification,*/ .logout:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
                cell.imvIcon.image = menuIcons[indexPath.row]
                cell.lblName.text = menusNames[indexPath.row]
                return cell
            }
        }
        return UITableViewCell()
    }
}
