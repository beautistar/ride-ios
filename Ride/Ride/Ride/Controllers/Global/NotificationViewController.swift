//
//  NotificationViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    weak var delegate: LeftMenuProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
       
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor(hex: Const.COLOR_WHITE)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationItem.title = Const.TITLE_NOTIFICATION
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: NotificationViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    @IBAction func backAction(_ sender: Any) {
        delegate?.changeViewController(LeftMenu.main)
    }
    
    
    
    func pushEnabledAtOSLevel() -> Bool {
        
        guard let currentSettings = UIApplication.shared.currentUserNotificationSettings?.types else { return false }
        return currentSettings.rawValue != 0
    }

}
