//
//  SelectViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class SelectViewController: BaseViewController {
    
    var email: String?
    var password: String?
    var user_type: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let email = UserDefaults.standard.string(forKey: Const.KEY_EMAIL) {
            self.email = email
            if let password = UserDefaults.standard.string(forKey: Const.KEY_PASSWORD) {
                self.password = password
                if let user_type = UserDefaults.standard.string(forKey: Const.KEY_USERTYPE) {
                    self.user_type = user_type
                    if email.count > 0 && password.count > 0 {
                        doLogin()
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doLogin() {
        
        self.showLoadingView()
        
        ApiRequest.login(email: email!, password: password!, user_type: user_type!) { (message) in
            self.hideLoadingView()
            if message == Const.RESULT_SUCCESS {
                
                if self.user_type == Const.DRIVER {
                    if currentUser?.is_approved != Const.APPROVED {
                        self.gotoThanksVC()
                    } else {
                        self.createMenuView()
                    }
                } else {
                    self.createMenuView()
                }
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
            }
        }
    }
    
    @IBAction func studentAction(_ sender: Any) {
        gotoMap(type: Const.STUDENT)
    }
    
    @IBAction func driverAction(_ sender: Any) {
        gotoDriverRegister()
    }
    
    // MARK: - Go    
    func gotoMap(type: String) {
        
        let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController        
        self.navigationController?.pushViewController(mainVC, animated: true)
        
    }
    
    func gotoDriverRegister() {
        let driverRegVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverRegisterViewController") as! DriverRegisterViewController
        self.navigationController?.pushViewController(driverRegVC, animated: true)
    }
    
    func gotoThanksVC() {
        let thanksVC = self.storyboard?.instantiateViewController(withIdentifier: "ThanksViewController") as! ThanksViewController
        self.navigationController?.pushViewController(thanksVC, animated: true)
    }
    
    fileprivate func createMenuView() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: Const.COLOR_WHITE)
        nvc.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        
        menuViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: menuViewController)
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        
    }
}
