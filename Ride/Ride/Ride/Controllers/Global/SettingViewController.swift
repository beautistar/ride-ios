//
//  SettingViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {
    
    weak var delegate: LeftMenuProtocol?
    @IBOutlet weak var alertMilleView: UIView!
    @IBOutlet weak var rideStart: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor(hex: Const.COLOR_WHITE)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if currentUser?.type == Const.DRIVER {
            alertMilleView.isHidden = false
            self.navigationItem.title = Const.TITLE_START_RIDE
        } else {
            alertMilleView.isHidden = true
            self.navigationItem.title = Const.TITLE_SETTING
        }
        
        if let user = currentUser {
            if user.is_started == Const.IS_STARTED {
                rideStart.setOn(true, animated: true)
            } else {
                rideStart.setOn(false, animated: true)
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: SettingViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    @IBAction func startRideAction(_ sender: Any) {
        
        let is_started = rideStart.isOn ? 1 : 0
        
        self.showLoadingView()
        ApiRequest.startRide(is_started) { (result_code) in
            
            self.hideLoadingView()
            
            if result_code == Const.CODE_SUCCESS {
                currentUser?.is_started = is_started
                if is_started == Const.IS_STARTED {
                    self.showToast(Const.MSG_START_RIDE)
                    currentUser?.is_started = is_started
                } else {
                    self.showToast(Const.MSG_STOP_RIDE)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.changeViewController(LeftMenu.main)
    }

}
