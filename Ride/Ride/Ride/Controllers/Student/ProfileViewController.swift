//
//  ProfileViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class ProfileViewController: BaseViewController {
    
    weak var delegate: LeftMenuProtocol?

    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfSchool: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var tfBus: UITextField!
    @IBOutlet weak var tfAlertMile: UITextField!
    @IBOutlet weak var tfCounty: UITextField!
    @IBOutlet weak var tfTownship: UITextField!
    
    var stateList = [AreaModel]()
    var countyList = [AreaModel]()
    var townshipList = [AreaModel]()
    var schoolList = [MarkerAnnotation]()
    var busList = [BusModel]()
    
    var user = UserModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor(hex: Const.COLOR_WHITE)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        initUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getState()
        getSchoolDistrict(school_id: currentUser!.school_id)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUserData() {
        
        user = currentUser!
        
        tfFirstName.text = user.first_name
        tfLastName.text = user.last_name
        tfPhone.text = user.phone_no
        tfSchool.text = user.school_name
        tfBus.text = user.bus_no
        tfAlertMile.text = "\(user.alert_mile)"
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }

    //MARK: - Button Action
    @IBAction func stateAction(_ sender: Any) {
        
        var rows = [String]()
        for area in stateList {
            rows.append(area.name)
        }
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECTSTATE, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfState.text = index as? String
            self.tfCounty.text = ""
            self.tfTownship.text = ""
            self.tfSchool.text = ""
            self.tfBus.text = ""
            self.user.state_id = self.stateList[value].id
            self.user.state_name = index as! String
            
            print("--selected state id : \(self.user.state_id) ----")
            print("--selected state name : \(self.user.state_name) ----")
            
            self.getCounty(state_id: self.stateList[value].id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func countyAction(_ sender: Any) {
        
        var rows = [String]()
        for area in countyList {
            rows.append(area.name)
        }
        
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_COUNTY, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfCounty.text = index as? String
            self.tfTownship.text = ""
            self.tfSchool.text = ""
            self.tfBus.text = ""
            self.user.county_id = self.countyList[value].id
            self.user.county_name = index as! String
            
            print("--selected county id : \(self.user.county_id) ----")
            print("--selected county name : \(self.user.county_name) ----")
            
            self.getTownship(county_id: self.user.county_id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func townshipAction(_ sender: Any) {
        
        var rows = [String]()
        for area in townshipList {
            rows.append(area.name)
        }
        
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_TOWNSHIP, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfTownship.text = index as? String
            self.tfSchool.text = ""
            self.tfBus.text = ""
            self.user.township_id = self.townshipList[value].id
            self.user.township_name = index as! String
            
            print("--selected township id : \(self.user.county_id) ----")
            print("--selected township name : \(self.user.county_name) ----")
            
            self.getSchool(township_id: self.user.township_id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    @IBAction func schoolAction(_ sender: Any) {
        
        var rows = [String]()
        for area in schoolList {
            rows.append(area.name)
        }
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_SCHOOL, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfSchool.text = index as? String
            self.tfBus.text = ""
            self.user.school_id = self.schoolList[value].id
            self.user.school_name = index as! String
            
            print("--selected school id : \(self.user.county_id) ----")
            print("--selected school name : \(self.user.county_name) ----")
            
            self.getBus(school_id: self.user.school_id)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func busAction(_ sender: Any) {
        
        var rows = [String]()
        for area in busList {
            rows.append(area.number!)
        }
        if rows.count == 0 {return};
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECTBUS, rows: rows, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfBus.text = index as? String
            self.user.bus_id = self.busList[value].id!
            self.user.bus_no = index as! String
            
            print("--selected bus id : \(self.user.bus_id) ----")
            print("--selected bus no : \(self.user.bus_no) ----")
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func alertMileAction(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_ALERTMILE, rows: alert_miles, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfAlertMile.text = index as? String
            self.user.alert_mile = value+1
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func updaetAction(_ sender: Any) {
        
        if isValid() {
            doUpdate()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.changeViewController(LeftMenu.main)
    }
    
    //MARK: - Private method
    func isValid() -> Bool {
        
        if tfFirstName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_FIRSTNAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfLastName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_LASTNAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPhone.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PHONE_NO_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfState.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_STATE, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfCounty.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_COUNTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfTownship.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_TOWNSHIP, positive: Const.OK, negative: nil)
            return false
        }
        
        
        if tfSchool.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_SCHOOL, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfBus.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_BUS, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfAlertMile.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_ALERTMILE, positive: Const.OK, negative: nil)
            return false
        }       
        
        return true
    }
    
    //MARK: - API manage
    
    func getSchoolDistrict(school_id: Int) {
        
        self.schoolList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getSchoolDistrict(school_id) { (res_code, schools, state_name, county_name, township_name) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.schoolList = schools
                self.tfState.text = state_name
                self.tfCounty.text = county_name
                self.tfTownship.text = township_name
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getState() {
        
        self.stateList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getState { (res_code, area) in
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.stateList = area
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getCounty(state_id: Int) {
        
        self.countyList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getCounty(state_id) { (res_code, counties) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.countyList = counties
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getTownship(county_id: Int) {
        
        self.townshipList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getTownship(county_id) { (res_code, townships) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.townshipList = townships
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getSchool(township_id: Int) {
        
        self.schoolList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getSchool(township_id) { (res_code, schools) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.schoolList = schools
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getBus(school_id: Int) {
        
        self.busList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getBus(school_id) { (res_code, buses) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.busList = buses
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func doUpdate() {
        
        user.first_name = tfFirstName.text!
        user.last_name = tfLastName.text!
        user.phone_no = tfPhone.text!
        user.password = tfPassword.text!
        user.alert_mile = Int(tfAlertMile.text!)!
        
        self.showLoadingView()
        
        ApiRequest.updateStudent(user) { (message) in
            
            self.hideLoadingView()
            
            if message != Const.RESULT_SUCCESS {
            
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            } else {
                currentUser?.first_name = self.user.first_name
                currentUser?.last_name = self.user.last_name
                currentUser?.phone_no = self.user.phone_no
                currentUser?.password = self.user.password
                currentUser?.school_id = self.user.school_id
                currentUser?.school_name = self.user.school_name
                currentUser?.bus_id = self.user.bus_id
                currentUser?.bus_no = self.user.bus_no
                currentUser?.alert_mile = self.user.alert_mile
                
                self.showToast(Const.MSG_UPDATE_SUCCESS)
            }
        }
        
    }
}
