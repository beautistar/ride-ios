//
//  SignupNameViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class SignupNameViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var photoBgView: UIView!
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfSchoolName: UITextField!
    @IBOutlet weak var imvProfile: UIImageView!
    
    var selectedSchool: MarkerAnnotation?
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgUrl: String = ""
    var pickedImage: UIImage?
    var user = UserModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        photoBgView.layer.borderColor = UIColor.white.cgColor
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        self.navigationItem.title = Const.TITLE_STD_REG
        
        self._picker.delegate = self
        _picker.allowsEditing = true
        
        tfSchoolName.text = selectedSchool?.name
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - ButtonMethod
    @IBAction func continueAction(_ sender: Any) {
        gotoPwdSignup()
    }
    
    //MARK: - Private method
    
    func isValid() -> Bool {
        
        if tfFirstName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_FIRSTNAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfLastName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_LASTNAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_INVALID, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPhone.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PHONE_NO_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfSchoolName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_SCHOOL, positive: Const.OK, negative: nil)
            return false
        }
        
        if _imgUrl.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PHOTO, positive: Const.OK, negative: nil)
            return false
            
        }
        
        return true
    }
    
    
    @IBAction func attachAction(_ sender: Any) {
    
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            || UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let galleryAction: UIAlertAction = UIAlertAction(title: Const.FROM_GALLERY, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let cameraAction: UIAlertAction = UIAlertAction(title: Const.FROM_CAMERA, style: UIAlertActionStyle.default, handler: { (cameraSourceAlert) in
                self._picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self._picker, animated: true, completion: nil)
            })
            
            
            photoSourceAlert.addAction(galleryAction)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                photoSourceAlert.addAction(cameraAction)
            }
            photoSourceAlert.addAction(UIAlertAction(title: Const.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            photoSourceAlert.popoverPresentationController?.sourceView = photoBgView
            photoSourceAlert.popoverPresentationController?.sourceRect = photoBgView.bounds
            self.present(photoSourceAlert, animated: true, completion: nil);
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.pickedImage = pickedImage
            self.imvProfile.image = self.pickedImage
            _imgUrl = saveToFile(image: pickedImage, filePath: Const.SAVE_ROOT_PATH, fileName: "profile.png")            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func gotoPwdSignup() {
        
        if isValid() {
            
            user.first_name = tfFirstName.text!
            user.last_name = tfLastName.text!
            user.email = tfEmail.text!
            user.phone_no = tfPhone.text!
            user.school_id = (self.selectedSchool?.id)!
            user.school_name = (self.selectedSchool?.name)!
            
            
            let pwdSignupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupPwdViewController") as! SignupPwdViewController
            pwdSignupVC.profileImage = self.pickedImage
            pwdSignupVC.profileImageUrl = _imgUrl
            pwdSignupVC.user = self.user
            pwdSignupVC.selectedSchool = self.selectedSchool
            self.navigationController?.pushViewController(pwdSignupVC, animated: true)
            
        }
    }
    
    @IBAction func gotoLogin(_ sender: Any) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVC.user_type = Const.STUDENT
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
