//
//  SignupPwdViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SignupPwdViewController: BaseViewController {
    
    @IBOutlet weak var photoBgView: UIView!
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var tfBus: UITextField!
    @IBOutlet weak var tfAlertMile: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    var profileImage: UIImage?
    var profileImageUrl: String?
    var selectedSchool: MarkerAnnotation?
    
    var user = UserModel()
    var busList = [BusModel]()
    
    var alertMile: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        photoBgView.layer.borderColor = UIColor.white.cgColor
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        imvProfile.image = profileImage
        
        getBus()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getBus()  {
        
        self.showLoadingView()
        
        ApiRequest.getBus((selectedSchool?.id)!) { (res_code, buses) in
            
            self.hideLoadingView()
            if res_code == Const.CODE_SUCCESS {
                self.busList = buses
                
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }        
    }

    @IBAction func selectBusAction(_ sender: Any) {
        
        var bus_nos = [String]()
        for bus in busList {
            bus_nos.append(bus.number!)
        }
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECTBUS, rows: bus_nos, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfBus.text = index as? String
            self.user.bus_id = self.busList[value].id!
            self.user.bus_no = index as! String
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    
    }
    
    @IBAction func alertMileAction(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: Const.TITLE_SELECT_ALERTMILE, rows: alert_miles, initialSelection: 0, doneBlock: {
            picker, value, index in
            self.tfAlertMile.text = index as? String
            self.user.alert_mile = value+1
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    
    }
    
    func isValid() -> Bool {
        
        if tfBus.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_BUS, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfAlertMile.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_ALERTMILE, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfConfirmPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_CONFIRM, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfConfirmPassword.text != tfPassword.text {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_CORRECT_PWD, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
        
        if isValid() {
            
            user.password = tfPassword.text!
            
            self.showLoadingView()
            ApiRequest.registerStudent(user, imgURL: profileImageUrl!) { (res_code) in
                
                ApiRequest.saveToken(token: currentUser!.token, completion: { (code) in
                })
                
                self.hideLoadingView()
                
                if res_code == Const.CODE_SUCCESS {
                    self.createMenuView()
                } else if res_code == Const.CODE_ALREADY_REGISTERED {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.EXIST_EMAIL, positive: Const.OK, negative: nil)
                } else if res_code == Const.CODE_UPLOADFAIL {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.UPLOAD_FAILED, positive: Const.OK, negative: nil)
                } else {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
                }
            }
        }        
    }
    
    
    fileprivate func createMenuView() {
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)        
        UINavigationBar.appearance().tintColor = UIColor(hex: Const.COLOR_WHITE)
        nvc.navigationBar.barTintColor = UIColor(hex: Const.COLOR_NAVYBLUE)
        
        menuViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: menuViewController)
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        
    }
}
