//
//  BusAnnotation.swift
//  Ride
//
//  Created by Yin on 2018/9/5.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class MarkerAnnotation: NSObject, MKAnnotation {
    
    var id: Int = 0
    var township_id = 0
    var name = ""
    var address = ""
    var latitude = 0.0
    var longitude = 0.0
    var is_started = 0
    let title: String?
    //let locationName: String
    //let discipline: String
    var photo_url = ""
    var coordinate: CLLocationCoordinate2D
    var type: String
    
    init(id: Int, title: String, address: String, type: String, photo_url: String, coordinate: CLLocationCoordinate2D, is_started: Int) {
        self.id = id
        self.name = title
        self.title = title
        self.address = address
        self.coordinate = coordinate
        self.type = type
        self.is_started = is_started
        self.photo_url = photo_url
        super.init()
    }
    
    var subtitle: String? {
        return address
    }
    

    // Annotation right callout accessory opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        return mapItem
    }
    
}
