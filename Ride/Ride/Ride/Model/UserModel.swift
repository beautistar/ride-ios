//
//  UserModel.swift
//  Transform
//
//  Created by Yin on 24/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation

class UserModel {

    var user_id: Int = 0
    var first_name = ""
    var last_name = ""
    var email = ""
    var password = ""
    var phone_no = ""
    var photo_url = ""
    var school_id = 0
    var school_name = ""
    var bus_id = 0
    var bus_no = ""
    var alert_mile = 0
    var type = ""
    
    // for driver
    var state_id = 0
    var state_name = ""
    var county_id = 0
    var county_name = ""
    var township_id = 0
    var township_name = ""
    var is_approved = 0
    var is_started = 1
    
    var latitude: Float {
        get {
            if let lat = UserDefaults.standard.value(forKey: Const.KEY_LAT) {
                return lat as! Float
            }
            return 0.0
        }
    }
    
    var longitude: Float {
        get {
            if let lng = UserDefaults.standard.value(forKey: Const.KEY_LNG) {
                return lng as! Float
            }
            return 0.0
        }
    }
    
    var token: String {
        get {
            if let token = UserDefaults.standard.value(forKey: Const.KEY_TOKEN) {
                return token as! String
            }
            return ""
        }
    }
}
