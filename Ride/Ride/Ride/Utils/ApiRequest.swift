//
//  ApiRequest.swift
//  Transform
//
//  Created by Yin on 24/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiRequest {
    
    static let BASE_URL                             = "http://52.23.80.212/"
    static let SERVER_URL                           = BASE_URL + "app/api/"
    
    static let REQ_REG_DRIVER                       = SERVER_URL + "registerDriver"
    static let REQ_REG_STUDENT                      = SERVER_URL + "registerStudent"
    static let REQ_EDIT_DRIVER                      = SERVER_URL + "editDriver"
    static let REQ_LOGIN_DRIVER                     = SERVER_URL + "loginDriver"
    static let REQ_LOGIN_STUDENT                    = SERVER_URL + "loginStudent"
    static let REQ_GETNEARBYSCHOOL                  = SERVER_URL + "getNearbySchool"
    static let REQ_GET_BUS                          = SERVER_URL + "getBus"
    
    static let REQ_GETSTATE                         = SERVER_URL + "getState"
    static let REQ_GETCOUNTY                        = SERVER_URL + "getCounty"
    static let REQ_GETTOWNSHIP                      = SERVER_URL + "getTownship"
    static let REQ_GETSCHOOL                        = SERVER_URL + "getSchool"
    static let REQ_GETSCHOOLDIRECTION               = SERVER_URL + "getSchoolDistrict"
    static let REQ_UPDATE_DRIVER                    = SERVER_URL + "updateDriverProfile"
    static let REQ_UPDATE_STUDENT                   = SERVER_URL + "updateStudentProfile"
    static let REQ_UPDATELOCATION                   = SERVER_URL + "updateDriverLocation"
    static let REQ_MY_DRIVER                        = SERVER_URL + "getMyDriver"
    static let REQ_START_RIDE                       = SERVER_URL + "startRide"
    static let REQ_REG_TOKEN                        = SERVER_URL + "registerToken"
    static let REQ_SEND_NOTIFICATION                = SERVER_URL + "sendNotification"
    
    static func getNearbySchool(_ count: Int, completion: @escaping(Int, [MarkerAnnotation]) -> ()) {
        
        var latitude: Float = 0.0
        var longitude: Float = 0.0
        var user_id = 0
        if let user = currentUser {
            user_id = user.user_id
            latitude = user.latitude
            longitude = user.longitude
        } else {
            if let lat = UserDefaults.standard.value(forKey: Const.KEY_LAT)  {
                latitude = lat as! Float
            }
            if let lng = UserDefaults.standard.value(forKey: Const.KEY_LNG)  {
                longitude = lng as! Float
            }
        }
        let params = [Const.PARAM_ID: user_id,
                      Const.PARAM_COUNT: count,
                      Const.PARAM_LATITUDE: latitude,
                      Const.PARAM_LONGITUDE: longitude] as [String : Any]
        
        Alamofire.request(REQ_GETNEARBYSCHOOL, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var schoolMarkers = [MarkerAnnotation]()
                    for schoolObject in json[Const.PARAM_SCHOOLLIST].arrayValue {
                        schoolMarkers.append(ParseHelper.parseSchoolAnnotation(schoolObject))
                    }
                    completion(Const.CODE_SUCCESS, schoolMarkers)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func getBus(_ school_id: Int, completion: @escaping(Int, [BusModel]) ->()) {
        
        let params = [Const.PARAM_SCHOOLID: school_id] as [String: Any]
        
        Alamofire.request(REQ_GET_BUS, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var buses = [BusModel]()
                    for busObject in json[Const.PARAM_BUS_LIST].arrayValue {
                        buses.append(ParseHelper.parseBus(busObject))
                    }
                    completion(Const.CODE_SUCCESS, buses)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }        
    }
    
    static func registerStudent(_ user: UserModel, imgURL: String, completion: @escaping (Int) -> ()) {
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath: imgURL), withName: Const.PARAM_PHOTO)
                multipartFormData.append(String(user.first_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_FIRSTNAME)
                multipartFormData.append(String(user.last_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_LASTNAME)
                multipartFormData.append(String(user.email).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_EMAIL)
                multipartFormData.append(String(user.phone_no).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_PHONENO)
                multipartFormData.append(String(user.password).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_PASSWORD)
                multipartFormData.append(String(user.school_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_SCHOOLNAME)
                multipartFormData.append(String(user.school_id).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_SCHOOLID)
                multipartFormData.append(String(user.bus_id).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_BUS_ID)
                multipartFormData.append(String(user.bus_no).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_BUS_NO)
                multipartFormData.append(String(user.alert_mile).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_ALERTMILE)
        },
            to: REQ_REG_STUDENT,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("---------------studnet register response -----------------")
                        print(response)
                        
                        switch response.result {
                            
                        case .success(_):
                            let json = JSON(response.result.value!)
                            let resCode = json[Const.RESULT_CODE].intValue
                            if (resCode == Const.CODE_SUCCESS) {
                                user.user_id = json[Const.PARAM_ID].intValue
                                user.photo_url = json[Const.PARAM_PHOTOURL].stringValue
                                
                                UserDefaults.standard.set(user.email, forKey: Const.KEY_EMAIL)
                                UserDefaults.standard.set(user.password, forKey: Const.KEY_PASSWORD)
                                UserDefaults.standard.set(Const.STUDENT, forKey: Const.KEY_USERTYPE)
                                
                                user.type = Const.STUDENT
                                
                                currentUser = user
                                
                                if duration > json[Const.PARAM_DURATION].intValue {
                                    duration = json[Const.PARAM_DURATION].intValue
                                    UserDefaults.standard.set(duration, forKey: Const.KEY_DURATION)
                                }
                                
                                completion(Const.CODE_SUCCESS)
                            }
                            else {
                                completion(resCode)
                            }
                        case .failure(_):
                            completion(Const.CODE_FAIL)
                        }
                    }
                    
                case .failure(_):
                    completion(Const.CODE_FAIL)
                }
        })
    }
    
    
    static func login(email: String, password: String, user_type: String, completion: @escaping (String) -> () ) {
        
        var url = ""
        var param: [String: Any]
        if user_type == Const.STUDENT {
            url = REQ_LOGIN_STUDENT
            param = [Const.PARAM_EMAIL : email, Const.PARAM_PASSWORD : password]
        } else {
            url = REQ_LOGIN_DRIVER
            param = [Const.PARAM_EMAIL : email, Const.PARAM_CODE : password]
        }
        
        Alamofire.request(url, method: .post, parameters: param).responseJSON { response in
            
            print("---------------\(user_type) login response -----------------")
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {                    
                    
                    if user_type == Const.STUDENT {
                        currentUser = ParseHelper.parseLoginStudent(json)
                    } else {
                        currentUser = ParseHelper.parseLoginDriver(json)
                    }
                    
                    currentUser?.type = user_type
                    currentUser?.password = password
                    UserDefaults.standard.set(email, forKey: Const.KEY_EMAIL)
                    UserDefaults.standard.set(password, forKey: Const.KEY_PASSWORD)
                    UserDefaults.standard.set(user_type, forKey: Const.KEY_USERTYPE)
                    
                    if duration > json[Const.PARAM_DURATION].intValue {
                        duration = json[Const.PARAM_DURATION].intValue
                        UserDefaults.standard.set(duration, forKey: Const.KEY_DURATION)
                    }
                    
                    completion(Const.RESULT_SUCCESS)
                    
                }
                else if resCode == Const.CODE_WRONGPWD {
                    completion(Const.INVALID_LOGIN)
                }
            }
        }
    }
    
    static func getState(_ completion: @escaping(Int, [AreaModel]) -> ()) {
        
        Alamofire.request(REQ_GETSTATE, method: .post).responseJSON { response in
            
            print("-----------State list---------------")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var area = [AreaModel]()
                    for areaObjc in json[Const.PARAM_STATELIST].arrayValue {
                        area.append(ParseHelper.parseArea(areaObjc))
                    }
                    completion(Const.CODE_SUCCESS, area)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func getCounty(_ state_id: Int, completion: @escaping(Int, [AreaModel]) -> ()) {
        
        Alamofire.request(REQ_GETCOUNTY, method: .post, parameters: [Const.PARAM_STATEID : state_id]).responseJSON { response in
            
            print("-----------county list---------------")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var area = [AreaModel]()
                    for areaObjc in json[Const.PARAM_COUNTYLIST].arrayValue {
                        area.append(ParseHelper.parseArea(areaObjc))
                    }
                    completion(Const.CODE_SUCCESS, area)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func getTownship(_ county_id: Int, completion: @escaping(Int, [AreaModel]) -> ()) {
        
        Alamofire.request(REQ_GETTOWNSHIP, method: .post, parameters: [Const.PARAM_COUNTYID : county_id]).responseJSON { response in
            
            print("-----------township list---------------")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var area = [AreaModel]()
                    for areaObjc in json[Const.PARAM_TOWNSHIPLIST].arrayValue {
                        area.append(ParseHelper.parseArea(areaObjc))
                    }
                    completion(Const.CODE_SUCCESS, area)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func getSchool(_ township_id: Int, completion: @escaping(Int, [MarkerAnnotation]) -> ()) {
        
        Alamofire.request(REQ_GETSCHOOL, method: .post, parameters: [Const.PARAM_TOWNSHIPID : township_id]).responseJSON { response in
            
            print("-----------school list---------------")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var schoolMarkers = [MarkerAnnotation]()
                    for schoolObject in json[Const.PARAM_SCHOOLLIST].arrayValue {
                        schoolMarkers.append(ParseHelper.parseSchoolAnnotation(schoolObject))
                    }
                    completion(Const.CODE_SUCCESS, schoolMarkers)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func registerDriver(_ user: UserModel, imgURL: String, completion: @escaping (Int) -> ()) {
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath: imgURL), withName: Const.PARAM_PHOTO)
                multipartFormData.append(String(user.first_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_FIRSTNAME)
                multipartFormData.append(String(user.last_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_LASTNAME)
                multipartFormData.append(String(user.email).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_EMAIL)
                multipartFormData.append(String(user.phone_no).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_PHONENO)
                multipartFormData.append(String(user.password).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_CODE)
                multipartFormData.append(String(user.school_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_SCHOOLNAME)
                multipartFormData.append(String(user.school_id).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_SCHOOLID)
                multipartFormData.append(String(user.bus_id).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_BUS_ID)
                multipartFormData.append(String(user.bus_no).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_BUS_NO)
                
                multipartFormData.append(String(user.state_id).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_STATEID)
                multipartFormData.append(String(user.state_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_STATENAME)
                multipartFormData.append(String(user.county_id).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_COUNTYID)
                multipartFormData.append(String(user.county_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_COUNTYNAME)
                multipartFormData.append(String(user.township_id).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_TOWNSHIPID)
                multipartFormData.append(String(user.township_name).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_TOWNSHIPNAME)
                
        },
            to: REQ_REG_DRIVER,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("---------------driver register response -----------------")
                        print(response)
                        
                        switch response.result {
                            
                        case .success(_):
                            let json = JSON(response.result.value!)
                            let resCode = json[Const.RESULT_CODE].intValue
                            if (resCode == Const.CODE_SUCCESS) {
                                user.user_id = json[Const.PARAM_ID].intValue
                                user.photo_url = json[Const.PARAM_PHOTOURL].stringValue
                                
                                UserDefaults.standard.set(user.email, forKey: Const.KEY_EMAIL)
                                UserDefaults.standard.set(user.password, forKey: Const.KEY_PASSWORD)
                                UserDefaults.standard.set(Const.DRIVER, forKey: Const.KEY_USERTYPE)
                                user.type = Const.DRIVER
                                currentUser = user
                                
                                if duration > json[Const.PARAM_DURATION].intValue {
                                    duration = json[Const.PARAM_DURATION].intValue
                                    UserDefaults.standard.set(duration, forKey: Const.KEY_DURATION)
                                }
                                
                                completion(Const.CODE_SUCCESS)
                            }
                            else {
                                completion(resCode)
                            }
                        case .failure(_):
                            completion(Const.CODE_FAIL)
                        }
                    }
                    
                case .failure(_):
                    completion(Const.CODE_FAIL)
                }
        })
    }
    
    static func updateStudent(_ user: UserModel, completion: @escaping (String) -> () ) {
        
        
        let params = [Const.PARAM_USERID: user.user_id,
                      Const.PARAM_FIRSTNAME: user.first_name,
                      Const.PARAM_LASTNAME: user.last_name,
                      Const.PARAM_PHONENO: user.phone_no,
                      Const.PARAM_PASSWORD: user.password,
                      Const.PARAM_SCHOOLID: user.school_id,
                      Const.PARAM_SCHOOLNAME: user.school_name,
                      Const.PARAM_BUS_ID: user.bus_id,
                      Const.PARAM_BUS_NO: user.bus_no,
                      Const.PARAM_ALERTMILE: user.alert_mile] as [String : Any]
        
        
        Alamofire.request(REQ_UPDATE_STUDENT, method: .post, parameters: params).responseJSON { response in
            
            print("---------------update student response -----------------")
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {                    
                    
                    UserDefaults.standard.set(user.password, forKey: Const.KEY_PASSWORD)
                    
                    completion(Const.RESULT_SUCCESS)
                    
                }
                else if resCode == Const.CODE_WRONGPWD {
                    completion(Const.INVALID_LOGIN)
                }
            }
        }
    }
    
    static func updateDriver(_ user: UserModel, completion: @escaping (String) -> () ) {
        
        
        let params = [Const.PARAM_USERID: user.user_id,
                      Const.PARAM_SCHOOLID: user.school_id,
                      Const.PARAM_SCHOOLNAME: user.school_name,
                      Const.PARAM_BUS_ID: user.bus_id,
                      Const.PARAM_BUS_NO: user.bus_no,
                      Const.PARAM_ISSTARTED: user.is_started] as [String : Any]
        
        
        Alamofire.request(REQ_UPDATE_DRIVER, method: .post, parameters: params).responseJSON { response in
            
            print("---------------update driver response -----------------")
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {                    
                    completion(Const.RESULT_SUCCESS)
                }
            }
        }
    }
    
    static func getSchoolDistrict(_ school_id: Int, completion: @escaping(Int, [MarkerAnnotation], String, String, String) -> ()) {
        
        Alamofire.request(REQ_GETSCHOOLDIRECTION, method: .post, parameters: [Const.PARAM_SCHOOLID : school_id]).responseJSON { response in
            
            print("-----------school distirct list---------------")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var schoolMarkers = [MarkerAnnotation]()
                    for schoolObject in json[Const.PARAM_SCHOOLLIST].arrayValue {
                        schoolMarkers.append(ParseHelper.parseSchoolAnnotation(schoolObject))
                    }
                    let state_name = json[Const.PARAM_AREAINFO][Const.PARAM_STATENAME].stringValue
                    let county_name = json[Const.PARAM_AREAINFO][Const.PARAM_COUNTYNAME].stringValue
                    let township_name = json[Const.PARAM_AREAINFO][Const.PARAM_TOWNSHIPNAME].stringValue
                    
                    completion(Const.CODE_SUCCESS, schoolMarkers,state_name, county_name, township_name)
                }
            } else {
                completion(Const.CODE_FAIL, [], "", "", "")
            }
        }
    }
    
    static func updateDriverLocation(_ completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_LATITUDE: currentUser!.latitude,
                      Const.PARAM_LONGITUDE: currentUser!.longitude] as [String : Any]
        
        Alamofire.request(REQ_UPDATELOCATION, method: .post, parameters: params).responseJSON { response in
            
            print("-----------update user location to server-----------")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    
                    completion(Const.CODE_SUCCESS)
                }
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func getMyDriver(_ completion: @escaping(Int, MarkerAnnotation) -> ()) {
        
        // working
        
        let param = [Const.PARAM_BUS_ID: currentUser!.bus_id] as [String: Any]
        
        Alamofire.request(REQ_MY_DRIVER, method: .post, parameters: param).responseJSON { (response) in
            
            print("-----------get driver-----------")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    
                    let busMarker = ParseHelper.parseBusAnnotation(json)
                    
                    completion(Const.CODE_SUCCESS, busMarker)
                }
            } else {
                //completion(Const.CODE_FAIL, nil)
            }            
        }
    }
    
    
    static func startRide(_ is_started: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_ISSTARTED: is_started] as [String: Any]
        
        Alamofire.request(REQ_START_RIDE, method: .post, parameters: params).responseJSON { (response) in
            
            print("-----------start ride-----------")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    
                    completion(Const.CODE_SUCCESS)
                }
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func sendNotification(userId: Int, message: String, completion: @escaping(Int) -> ()) {
        
        var user_type = 0
        if currentUser!.type == Const.STUDENT {
            user_type = 1
        }
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_USERTYPE: user_type,
                      Const.PARAM_BODY: message,
                      Const.PARAM_CONTENT: message] as [String : Any]
        
        Alamofire.request(REQ_SEND_NOTIFICATION, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                completion(Const.CODE_SUCCESS)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func saveToken(token: String, completion: @escaping(Int) -> ()) {
        if let user = currentUser {
            var user_type = 0
            if user.type == Const.STUDENT {
                user_type = 1
            }
            let params = [Const.PARAM_USERID: currentUser!.user_id,
                          Const.PARAM_TOKEN: token,
                          Const.PARAM_USERTYPE: user_type] as [String: Any]
            
            Alamofire.request(REQ_REG_TOKEN, method: .post, parameters: params).responseJSON { response in
                
                print(response)
                
                if response.result.isSuccess {
                    completion(Const.CODE_SUCCESS)
                } else {
                    completion(Const.CODE_FAIL)
                }
            }
        }
    }
}
















