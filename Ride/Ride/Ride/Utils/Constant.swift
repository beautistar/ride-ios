//
//  Constant.swift
//  Ride
//
//  Created by Yin on 2018/9/6.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation



class Const {
    
    static let APP_NAME                     = "Ride"
    static let OK                           = "OK"
    static let CANCEL                       = "Cancel"
    static let NO                           = "No"
    static let YES                          = "Yes"
    static let SAVE_ROOT_PATH               = "Ride"
    
    //MARK: - Color code
    static let COLOR_NAVYBLUE               = "04063a"
    static let COLOR_TEAL                   = "86e0d1"
    static let COLOR_WHITE                  = "ffffff"
    
    //MARK: - MapView
    static let MARKER_BUS                   = "Bus"
    static let MARKER_SCHOOL                = "School"
    
    // User type
    static let STUDENT                      = "student"
    static let DRIVER                       = "driver"
    
    //error messages
    
    static let CHECK_FIRSTNAME_EMPTY        = "Please input your first name"
    static let CHECK_LASTNAME_EMPTY         = "Please input your last name"
    static let CHECK_PASSWORD_EMPTY         = "Please input your password"
    static let CHECK_CODE_EMPTY             = "Please input your code"
    static let CHECK_PASSWORD_CONFIRM       = "Please input confirm password"
    static let CHECK_CORRECT_PWD            = "Password does not match"
    static let CHECK_PHONE_NO_EMPTY         = "Please input your phone number"
    static let CHECK_EMAIL_EMPTY            = "Please input your email address"
    static let CHECK_EMAIL_INVALID          = "Please input valid email"
    static let CHECK_PHOTO                  = "Please select your profile photo"
    static let SELECT_STATE                 = "Please select state"
    static let SELECT_COUNTY                = "Please select county"
    static let SELECT_TOWNSHIP              = "Please select township"
    static let SELECT_SCHOOL                = "Please select school"
    static let SELECT_BUS                   = "Please select school bus"
    static let SELECT_ALERTMILE             = "Please select alert mile"
    static let TITLE_SELECTSTATE            = "Select state"
    static let TITLE_SELECT_COUNTY          = "Select county"
    static let TITLE_SELECT_TOWNSHIP        = "Select township"
    static let TITLE_SELECT_SCHOOL          = "Select school"
    static let TITLE_SELECTBUS              = "Select Bus"
    static let TITLE_SELECT_ALERTMILE       = "Select alert mile"
    
    static let TITLE_STD_REG                = "STUDENT REGISTER"
    static let TITLE_DRV_REG                = "OPERATOR REGISTER"
    static let TITLE_DRV_LOGIN              = "OPERATOR LOGIN"
    static let TITLE_STD_LOGIN              = "STUDENT LOGIN"
    static let TITLE_SETTING                = "SETTING"
    static let TITLE_START_RIDE             = "START RIDE"
    static let TITLE_NOTIFICATION           = "NOTIFICATION"
    
    static let ERROR_CONNECT                = "Failed to server connection"
    static let EXIST_EMAIL                  = "User is already registered"
    static let UPLOAD_FAILED                = "Upload failed"
    
    static let UNREGISTERED_USER            = "User does not registered"
    static let INVALID_LOGIN                = "Invalid Email or Password!"
    static let SCHOOL_NOT_FOUND             = "School did not found"
    static let MSG_UPDATE_SUCCESS           = "Update succeed!"
    static let MSG_START_RIDE               = "Please start riding your bus"
    static let MSG_STOP_RIDE                = "You stopped your bus riding."
    
    
    // Params
    
    static let PARAM_COUNT                  = "count"
    static let PARAM_LATITUDE               = "latitude"
    static let PARAM_LONGITUDE              = "longitude"    
    static let PARAM_ID                     = "id"
    static let PARAM_NAME                   = "name"
    static let PARAM_ADDRESS                = "address"
    
    static let PARAM_SCHOOLLIST             = "school_list"
    static let PARAM_SCHOOLID               = "school_id"
    static let PARAM_SCHOOLNAME             = "school_name"
    
    static let PARAM_BUS_LIST               = "bus_list"
    static let PARAM_BUS_ID                 = "bus_id"
    static let PARAM_BUS_NO                 = "bus_no"
    static let PARAM_NUMBER                 = "number"
    
    
    static let PARAM_USERMODEL              = "user_model"
    static let PARAM_USERID                 = "user_id"
    static let PARAM_FIRSTNAME              = "first_name"
    static let PARAM_LASTNAME               = "last_name"
    static let PARAM_EMAIL                  = "email"
    static let PARAM_PASSWORD               = "password"
    static let PARAM_CODE                   = "code"
    static let PARAM_PHONENO                = "phone_number"
    static let PARAM_PHOTOURL               = "photo_url"
    static let PARAM_PHOTO                  = "photo"
    static let PARAM_ALERTMILE              = "alert_mile"
    static let PARAM_ISAPPROVED             = "is_approved"
    static let PARAM_ISSTARTED              = "is_started"
    static let PARAM_DURATION               = "duration"
    static let PARAM_USERTYPE               = "user_type"
    static let PARAM_TOKEN                  = "token"
    static let PARAM_BODY                   = "body"
    static let PARAM_CONTENT                = "content"
    
    static let PARAM_STATEID                = "state_id"
    static let PARAM_STATENAME              = "state_name"
    static let PARAM_COUNTYID               = "county_id"
    static let PARAM_COUNTYNAME             = "county_name"
    static let PARAM_TOWNSHIPID             = "township_id"
    static let PARAM_TOWNSHIPNAME           = "township_name"
    static let PARAM_STATELIST              = "state_list"
    static let PARAM_COUNTYLIST             = "county_list"
    static let PARAM_TOWNSHIPLIST           = "township_list"
    
    static let PARAM_AREAINFO               = "area_info"
    
    static let FROM_GALLERY                 = "Gallery"
    static let FROM_CAMERA                  = "Camera"
    
    static let APPROVED                     = 1
    static let IS_STARTED                   = 1
    static let IS_NOT_STARTED               = 0
    

    
    // Key
    static let KEY_USERID                   = "k_userid"
    static let KEY_TOKEN                    = "k_token"
    static let KEY_EMAIL                    = "k_email"
    static let KEY_PASSWORD                 = "k_password"
    static let KEY_LAT                      = "k_lat"
    static let KEY_LNG                      = "k_lng"
    static let KEY_ISLOGIN                  = "k_islogin"
    static let KEY_USERTYPE                 = "k_user_type"
    static let KEY_DURATION                 = "k_duration"
    static let KEY_MAP_MODE                 = "k_map_mode"
    
    // Result code
    static let RESULT_CODE                  = "result_code"
    static let RESULT_SUCCESS               = "result_success"
    static let CODE_FAIL                    = -100
    static let CODE_SUCCESS                 = 200
    
    static let CODE_ALREADY_REGISTERED      = 201
    static let CODE_UPLOADFAIL              = 206
    static let CODE_WRONGPWD                = 202
    
    

    
}
