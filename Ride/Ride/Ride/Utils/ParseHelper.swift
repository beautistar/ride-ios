//
//  ParseHelper.swift
//  Transform
//
//  Created by Yin on 26/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import MapKit
import Foundation
import SwiftyJSON

class ParseHelper {
    
    static func parseLoginStudent(_ json: JSON) -> UserModel {
        
        let user = UserModel()
        let userObject = json[Const.PARAM_USERMODEL]
        user.user_id = userObject[Const.PARAM_USERID].intValue
        user.first_name = userObject[Const.PARAM_FIRSTNAME].stringValue
        user.last_name = userObject[Const.PARAM_LASTNAME].stringValue
        user.phone_no = userObject[Const.PARAM_PHONENO].stringValue
        user.email = userObject[Const.PARAM_EMAIL].stringValue
        user.photo_url = userObject[Const.PARAM_PHOTOURL].stringValue
        user.alert_mile = userObject[Const.PARAM_ALERTMILE].intValue
        user.school_name = userObject[Const.PARAM_SCHOOLNAME].stringValue
        user.school_id = userObject[Const.PARAM_SCHOOLID].intValue
        user.bus_id = userObject[Const.PARAM_BUS_ID].intValue
        user.bus_no = userObject[Const.PARAM_BUS_NO].stringValue
        
        
        return user
    }
    
    static func parseLoginDriver(_ json: JSON) -> UserModel {
        
        let user = UserModel()
        let userObject = json[Const.PARAM_USERMODEL]
        user.user_id = userObject[Const.PARAM_USERID].intValue
        user.first_name = userObject[Const.PARAM_FIRSTNAME].stringValue
        user.last_name = userObject[Const.PARAM_LASTNAME].stringValue
        user.phone_no = userObject[Const.PARAM_PHONENO].stringValue
        user.email = userObject[Const.PARAM_EMAIL].stringValue
        user.photo_url = userObject[Const.PARAM_PHOTOURL].stringValue
        user.alert_mile = userObject[Const.PARAM_ALERTMILE].intValue
        user.school_name = userObject[Const.PARAM_SCHOOLNAME].stringValue
        user.school_id = userObject[Const.PARAM_SCHOOLID].intValue
        user.bus_id = userObject[Const.PARAM_BUS_ID].intValue
        user.bus_no = userObject[Const.PARAM_BUS_NO].stringValue
        
        
        //additional params
        user.is_approved = userObject[Const.PARAM_ISAPPROVED].intValue
        user.is_started = userObject[Const.PARAM_ISSTARTED].intValue
        user.state_id = userObject[Const.PARAM_STATEID].intValue
        user.state_name = userObject[Const.PARAM_STATENAME].stringValue
        user.county_id = userObject[Const.PARAM_COUNTYID].intValue
        user.county_name = userObject[Const.PARAM_COUNTYNAME].stringValue
        user.township_id = userObject[Const.PARAM_TOWNSHIPID].intValue
        user.township_name = userObject[Const.PARAM_TOWNSHIPNAME].stringValue
        
        return user
    }
    
    static func parseSchoolAnnotation(_ json: JSON) -> MarkerAnnotation {
        
        return MarkerAnnotation(id: json[Const.PARAM_ID].intValue,
                                title: json[Const.PARAM_NAME].stringValue,
                                address: json[Const.PARAM_ADDRESS].stringValue,
                                type: Const.MARKER_SCHOOL,
                                photo_url: "",
                                coordinate: CLLocationCoordinate2D(latitude: json[Const.PARAM_LATITUDE].doubleValue, longitude: json[Const.PARAM_LONGITUDE].doubleValue),
                                is_started: 0)
    }
    
    static func parseBus(_ json: JSON) -> BusModel {
        
        let bus = BusModel()
        bus.id = json[Const.PARAM_ID].intValue
        bus.number = json[Const.PARAM_NUMBER].stringValue
        
        return bus
        
    }
    
    static func parseArea(_ json: JSON) -> AreaModel {
        let area = AreaModel()
        area.id = json[Const.PARAM_ID].intValue
        area.name = json[Const.PARAM_NAME].stringValue
        return area
    }
    
    static func parseBusAnnotation(_ json: JSON) -> MarkerAnnotation {
        
        let driverObject = json[Const.PARAM_USERMODEL]
        
        return MarkerAnnotation(id: driverObject[Const.PARAM_ID].intValue,
                                title: driverObject[Const.PARAM_FIRSTNAME].stringValue + " " + driverObject[Const.PARAM_LASTNAME].stringValue,
                                address: "School Name: " + driverObject[Const.PARAM_SCHOOLNAME].stringValue +
                                         "\nBus number: " + driverObject[Const.PARAM_BUS_NO].stringValue,
                                type: Const.MARKER_BUS,
                                photo_url: driverObject[Const.PARAM_PHOTOURL].stringValue,
                                coordinate: CLLocationCoordinate2D(latitude: driverObject[Const.PARAM_LATITUDE].doubleValue, longitude: driverObject[Const.PARAM_LONGITUDE].doubleValue),
                                is_started: driverObject[Const.PARAM_ISSTARTED].intValue)
    }    
}
