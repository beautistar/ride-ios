//
//  BusAnnotationView.swift
//  Ride
//
//  Created by Yin on 2018/9/6.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import MapKit
import SDWebImage

class MarkerAnnotationView: MKAnnotationView {
    
    override var annotation: MKAnnotation? {
        willSet {
            guard let markerAnnotation = newValue as? MarkerAnnotation else {return}
            
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let signupButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 80, height: 30)))
            signupButton.backgroundColor = UIColor(hex: Const.COLOR_TEAL)
            signupButton.setTitle("Sign up", for: .normal)
            if let user = currentUser {
                if user.user_id == 0 {
                    rightCalloutAccessoryView = signupButton
                }                
            } else {
                rightCalloutAccessoryView = signupButton
            }
            
            if markerAnnotation.type == Const.MARKER_BUS {
                image = #imageLiteral(resourceName: "pin_bus_32")
                
                let photoImageView = UIImageView.init(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 50, height: 50)))
                photoImageView.contentMode = .scaleAspectFit
                photoImageView.sd_setImage(with: URL(string: markerAnnotation.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
                leftCalloutAccessoryView = photoImageView
                
            } else {
                image = #imageLiteral(resourceName: "pin_school_32")
                
            }            
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = markerAnnotation.address
            detailCalloutAccessoryView = detailLabel
        }
    }
}
